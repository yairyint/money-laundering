import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

Vue.use(VueRouter)

/* Guest Component */
const Login = () => import('../components/Login.vue' /* webpackChunkName: "resource/js/components/login" */)
const Register = () => import('../components/Register.vue' /* webpackChunkName: "resource/js/components/register" */)
/* Guest Component */

/* Layouts */
const DahboardLayout = () => import('../components/layouts/Dashboard.vue' /* webpackChunkName: "resource/js/components/layouts/dashboard" */)
const HomeLayout = () => import('../components/layouts/Home.vue' /* webpackChunkName: "resource/js/components/layouts/dashboard" */)
/* Layouts */

/* Authenticated Component */
const Dashboard = () => import('../components/content/Dashboard.vue' /* webpackChunkName: "resource/js/components/dashboard" */)
const Home = () => import('../components/content/Home.vue' /* webpackChunkName: "resource/js/components/dashboard" */)
const Blog = () => import('../components/content/Blog/index.vue' /* webpackChunkName: "resource/js/components/dashboard" */)
const BlogListsBody = () => import('../components/content/Blog/listsBody.vue' /* webpackChunkName: "resource/js/components/dashboard" */)
const BlogDetail = () => import('../components/content/Blog/detail.vue' /* webpackChunkName: "resource/js/components/dashboard" */)
/* Authenticated Component */


const Routes = [
    {
        name: "login",
        path: "/login",
        component: Login,
        meta:{
            middleware: "guest",
            title: `Login`
        }
    },
    {
        name: "register",
        path: "/register",
        component: Register,
        meta:{
            middleware: "guest",
            title: `Register`
        }
    },
    {
        path: "/",
        component: HomeLayout,
        meta: {
            middleware: "auth"
        },
        children: [
            {
                name: "dashboard",
                path: '/',
                component: Home,
                meta: {
                    title: `Home`
                }
            },
            {
                name: "blogs",
                path: '/blogs',
                component: Blog,
                meta: {
                    title: `Blog`
                },
                children: [
                    {
                        name: "list-body",
                        path: '/',
                        component: BlogListsBody,
                        meta: {
                            title: `Blog`
                        }
                    },
                    {
                        name: "blog-detail",
                        path: '/blog/:id',
                        component: BlogDetail,
                        meta: {
                            title: `Blog`
                        }
                    },

                ]
            }
        ]
    }
]

var router  = new VueRouter({
    mode: 'history',
    routes: Routes,
    scrollBehavior (to, from, savedPosition) {
        return { top: 0 }
    }
})

router.beforeEach((to, from, next) => {
    document.title = 'KBZ - Money Laundering'
    next()
    window.scroll({
        top: 0,
        left: 100,
        behavior: 'smooth'
    });
})

export default router
