import Vue from 'vue'
import axios from 'axios'

export default {
    namespaced: true,
    state: {
        blogLists: [],
        categories: [],
        tags: [],
        noMore: false,
    },
    getters: {
        getBlogLists (state) {
            return state.blogLists
        },
        getCategories (state) {
            return state.categories
        },
        getTags (state) {
            return state.tags
        },
        getNoMore (state) {
            return state.noMore
        },
    },
    mutations: {
        SET_BLOG_LISTS (state, payload) {
            Vue.set(state, 'blogLists', payload)
        },
        SET_CATEGORIES (state, payload) {
            Vue.set(state, 'categories', payload)
        },
        SET_TAGS (state, payload) {
            Vue.set(state, 'tags', payload)
        },
        SET_NO_MORE (state, payload) {
            Vue.set(state, 'noMore', payload)
        },
    },
    actions: {
        setBlogLists (context, payload) {
            let requestUrl = '/api/get/articles'
            if (payload && payload.page) {
                requestUrl += '?page=' + payload.page + '&limit=' + payload.limit
            }
            axios.get(requestUrl)
            .then(response => {
                if (payload && payload.page > 1) {
                    context.commit('SET_NO_MORE', payload.page === response.data.last_page)
                    let allBlogs = [...context.getters.getBlogLists, ...response.data.data]
                    context.commit('SET_BLOG_LISTS', allBlogs)
                } else {
                    let allBlogs = response.data.data
                    context.commit('SET_BLOG_LISTS', allBlogs)
                }
            }).catch(error=>{
                console.error(error)
            })
        },
        setCategories (context, payload) {
            axios.get('/api/get/categories')
            .then(response => {
                context.commit('SET_CATEGORIES', response.data)
            }).catch(error=>{
            })
        },
        setTags (context, payload) {
            axios.get('/api/get/tags')
            .then(response => {
                context.commit('SET_TAGS', response.data)
            }).catch(error=>{
            })
        },
    }
}
