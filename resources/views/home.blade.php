<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Free Web tutorials">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Ye Yint">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ config('app.name','Laravel')  }}</title>

    <!-- Style -->
    <link rel="icon" href="{{ url('images/favicon.ico') }}">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{ mix('css/responsive.css') }}" rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">
</head>
<body>
<div id="app">
    <router-view></router-view>
</div>

<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
