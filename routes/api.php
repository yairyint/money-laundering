<?php

use App\Http\Controllers\Api\ArticleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/get/articles', 'App\Http\Controllers\Api\ArticleController@getArticlesWithPagination');
Route::get('/get/articles/{id}', 'App\Http\Controllers\Api\ArticleController@getArticleById');
Route::get('/get/categories', 'App\Http\Controllers\Api\CategoryController@getCategories');
Route::get('/get/tags', 'App\Http\Controllers\Api\TagController@getTags');
