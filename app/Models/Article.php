<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Traits\Resizable;
use TCG\Voyager\Traits\Translatable;
use DateTimeInterface;

class Article extends Model
{
    use HasFactory;
    use Translatable;
    use Resizable;

    protected $fillable = [
        "title",
        'content',
        'slug',
        'featured_image',
        'is_published',
    ];

    public function user ()
    {
        return $this->belongsTo(User::class);
    }

    public function category ()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags ()
    {
        return $this->belongsToMany(Tag::class, 'article_tag');
    }

    public function getFeaturedImageAttribute ($value)
    {
        if (!$value) {
            return 'https://kbz-temp.s3.ap-south-1.amazonaws.com/users/default.png';
        }
        return Storage::url($value);
    }

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
