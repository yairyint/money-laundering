<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function getArticlesWithPagination (Request $request)
    {
        $page = ($request->has('page')) ? $request->input('page') : 1;
        $limit = ($request->has('limit')) ? $request->input('limit') : 6;

        $articles = Article::with(['user', 'category', 'tags'])
            ->where('is_published', true)
            ->orderBy('updated_at')
            ->paginate($limit, ['*'], 'page', $page);
        return response($articles->jsonSerialize(), 200);
    }

    public function getArticleById (Request $request, $id)
    {
        if ($id) {
            $article = Article::with(['user', 'category', 'tags'])
                                ->where('id', $id)
                                ->where('is_published', true)->first();
            return response($article->jsonSerialize(), 200);
        }
        return null;
    }
}
