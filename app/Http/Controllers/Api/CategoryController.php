<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function getCategories (Request $request)
    {
        $categories = Category::withCount('articles')->get();
        return response($categories->jsonSerialize(), 200);
    }
}
